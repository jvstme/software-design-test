from typing import Annotated

from fastapi import APIRouter, HTTPException, status
from prometheus_client import Counter
from pydantic import BaseModel, Field

from ..db import DB, OrderId
from ..product import service as product_service
from ..user.auth import AuthorizedUser
from .aggregate import Order, OrderBooking, OrderState
from .events import (
    CreateOrderEvent,
    DeleteOrderEvent,
    SetOrderBookingEvent,
    SetOrderItemEvent,
)

router = APIRouter()

created_orders = Counter("shop_orders_created", "Count of created orders")
deleted_orders = Counter("shop_orders_deleted", "Count of deleted orders")
booked_products = Counter("shop_booked_products", "Count of booked products")
booked_orders = Counter("shop_booked_orders", "Count of booked orders")


class SetOrderItemDto(BaseModel):
    quantity: Annotated[int, Field(ge=0)]


class ItemView(BaseModel):
    product_id: int
    product_name: str
    quantity: int


class OrderView(BaseModel):
    id: int
    owner_id: int = 0
    items: list[ItemView]
    booking: OrderBooking | None
    state: OrderState

    @staticmethod
    def from_order(order: Order, db: DB) -> "OrderView":
        items = []

        for product_id, item in order.items.items():
            product = product_service.get_product(product_id, db)

            if not product:
                continue

            item_view = ItemView(
                product_id=product_id, product_name=product.name, quantity=item.quantity
            )
            items.append(item_view)

        return OrderView(
            id=order.id,
            owner_id=order.owner_id,
            items=items,
            booking=order.booking,
            state=order.state,
        )


def get_owned_order(id: int, user: AuthorizedUser, db: DB) -> Order:
    order = Order.load(db, id)

    if not order.exists() or user.id != order.owner_id:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            f"Order {id} not found or belongs to a different user",
        )

    return order


def book_order(order: Order, db: DB) -> None:
    booked_count = 0

    for product_id, item in order.items.items():
        product = product_service.get_product(product_id, db)

        if not product:
            continue

        booked = product_service.try_decrease_quantity_available(product, item.quantity)

        if not booked:
            raise HTTPException(
                status.HTTP_409_CONFLICT,
                (
                    f"Cannot book {item.quantity} items of product {product_id}, only "
                    f"{product.quantity_available} items available"
                ),
            )

        booked_count += 1

    if not booked_count:
        raise HTTPException(status.HTTP_409_CONFLICT, "Cannot book empty order")

    booked_products.inc(booked_count)


def unbook_order(order: Order, db: DB) -> None:
    for product_id, item in order.items.items():
        product = product_service.get_product(product_id, db)

        if product:
            product_service.increase_quantity_available(product, item.quantity)


@router.get("/{id}")
def get_order(id: int, user: AuthorizedUser, db: DB) -> OrderView:
    return OrderView.from_order(get_owned_order(id, user, db), db)


@router.post("/")
def create_order(user: AuthorizedUser, db: DB) -> OrderView:
    order_id_record = OrderId()
    db.add(order_id_record)
    db.flush()
    id = order_id_record.id

    event = CreateOrderEvent(owner_id=user.id)
    order = Order(id=id)
    order.apply_create(event)

    event_record = event.to_event_record(id)
    db.add(event_record)
    db.flush()

    created_orders.inc()

    return OrderView.from_order(order, db)


@router.delete("/{id}")
def delete_order(id: int, user: AuthorizedUser, db: DB) -> OrderView:
    order = get_owned_order(id, user, db)

    if order.state == OrderState.booked:
        unbook_order(order, db)

    event = DeleteOrderEvent()
    order.apply_delete(event)

    event_record = event.to_event_record(id)
    db.add(event_record)
    db.flush()

    deleted_orders.inc()

    return OrderView.from_order(order, db)


@router.put("/{order_id}/items/{product_id}")
def set_order_item(
    order_id: int,
    product_id: int,
    item: SetOrderItemDto,
    user: AuthorizedUser,
    db: DB,
) -> OrderView:
    order = get_owned_order(order_id, user, db)
    product = product_service.get_product(product_id, db)

    if not product:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND, f"Product {product_id} not found"
        )

    event = SetOrderItemEvent(product_id=product_id, quantity=item.quantity)
    order.apply_set_item(event)

    event_record = event.to_event_record(order_id)
    db.add(event_record)
    db.flush()

    return OrderView.from_order(order, db)


@router.put("/{id}/booking")
def set_order_booking(
    id: int, booking: OrderBooking, user: AuthorizedUser, db: DB
) -> OrderView:
    order = get_owned_order(id, user, db)

    if order.state != OrderState.booked:
        book_order(order, db)
        booked_orders.inc()

    event = SetOrderBookingEvent(target_date=booking.target_date)
    order.apply_set_booking(event)

    event_record = event.to_event_record(id)
    db.add(event_record)
    db.flush()

    return OrderView.from_order(order, db)
