from abc import ABC, abstractstaticmethod
from datetime import datetime

from pydantic import BaseModel

from ..db import OrderEvent as OrderEventRecord


class OrderEvent(ABC, BaseModel):
    @abstractstaticmethod
    def kind() -> str:
        pass

    def to_event_record(self, order_id: int) -> OrderEventRecord:
        return OrderEventRecord(
            order_id=order_id,
            kind=self.kind(),
            data=self.json(),
        )


class CreateOrderEvent(OrderEvent):
    owner_id: int

    @staticmethod
    def kind() -> str:
        return "create"


class DeleteOrderEvent(OrderEvent):
    @staticmethod
    def kind() -> str:
        return "delete"


class SetOrderItemEvent(OrderEvent):
    product_id: int
    quantity: int

    @staticmethod
    def kind() -> str:
        return "set_item"


class SetOrderBookingEvent(OrderEvent):
    target_date: datetime

    @staticmethod
    def kind() -> str:
        return "set_booking"
