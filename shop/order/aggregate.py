from datetime import datetime
from enum import Enum
from json import JSONDecodeError

from pydantic import BaseModel, ValidationError
from sqlalchemy.orm import Session

from ..exceptions import InternalError, InvariantError
from .events import (
    CreateOrderEvent,
    DeleteOrderEvent,
    OrderEventRecord,
    SetOrderBookingEvent,
    SetOrderItemEvent,
)


class OrderState(str, Enum):
    initial = "initial"
    collecting = "collecting"
    deleted = "deleted"
    booked = "booked"


class OrderItem(BaseModel):
    quantity: int


class OrderBooking(BaseModel):
    target_date: datetime


class Order(BaseModel):
    id: int
    owner_id: int = 0
    items: dict[int, OrderItem] = {}  # product id to item
    booking: OrderBooking | None
    state: OrderState = OrderState.initial

    def exists(self) -> bool:
        return self.state not in (OrderState.initial, OrderState.deleted)

    def apply_create(self, event: CreateOrderEvent) -> None:
        if self.state != OrderState.initial:
            raise InvariantError(
                f"Can only create order from state {OrderState.initial}, "
                f"not {self.state}"
            )

        self.owner_id = event.owner_id
        self.state = OrderState.collecting

    def apply_set_item(self, event: SetOrderItemEvent) -> None:
        if self.state != OrderState.collecting:
            raise InvariantError(
                f"Can only set items on order state {OrderState.collecting}, "
                f"not {self.state}"
            )

        if not event.quantity:
            self.items.pop(event.product_id, None)
        else:
            item = OrderItem(quantity=event.quantity)
            self.items[event.product_id] = item

    def apply_set_booking(self, event: SetOrderBookingEvent) -> None:
        if self.state not in (OrderState.collecting, OrderState.booked):
            raise InvariantError(
                f"Can only set booking on order state {OrderState.collecting} or "
                f"{OrderState.booked}, not {self.state}"
            )

        self.state = OrderState.booked
        self.booking = OrderBooking(target_date=event.target_date)

    def apply_delete(self, _: DeleteOrderEvent) -> None:
        if not self.exists():
            raise InvariantError(f"Cannot delete order with state {self.state}")

        self.state = OrderState.deleted

    def apply(self, event_record: OrderEventRecord) -> None:
        try:
            if event_record.kind == CreateOrderEvent.kind():
                event = CreateOrderEvent.parse_raw(event_record.data)
                method = self.apply_create

            elif event_record.kind == SetOrderItemEvent.kind():
                event = SetOrderItemEvent.parse_raw(event_record.data)
                method = self.apply_set_item

            elif event_record.kind == DeleteOrderEvent.kind():
                event = DeleteOrderEvent.parse_raw(event_record.data)
                method = self.apply_delete

            elif event_record.kind == SetOrderBookingEvent.kind():
                event = SetOrderBookingEvent.parse_raw(event_record.data)
                method = self.apply_set_booking

            else:
                raise InternalError(f"Unknown event kind {event_record.kind}")

        except (ValidationError, JSONDecodeError):
            raise InternalError("Failed to parse event data")

        method(event)

    @staticmethod
    def load(db: Session, id: int) -> "Order":
        query = db.query(OrderEventRecord).where(OrderEventRecord.order_id == id)
        event_records = db.execute(query).scalars()

        order = Order(id=id)

        for event in event_records:
            order.apply(event)

        return order
