class InvariantError(Exception):
    pass


class InternalError(Exception):
    pass
