from secrets import compare_digest
from typing import Annotated

from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from ..settings import Settings

http_basic = HTTPBasic(scheme_name="Admin Auth")


def check_admin_auth(
    credentials: Annotated[HTTPBasicCredentials, Depends(http_basic)],
    settings: Settings,
) -> None:
    username = credentials.username.encode()
    password = credentials.password.encode()
    expected_username = settings.admin_username.encode()
    expected_password = settings.admin_password.encode()

    username_correct = compare_digest(username, expected_username)
    password_correct = compare_digest(password, expected_password)

    if not (username_correct and password_correct):
        raise HTTPException(
            status.HTTP_401_UNAUTHORIZED,
            "Incorrect username or password",
            {"WWW-Authenticate": "Basic"},
        )


admin_auth = Depends(check_admin_auth)
