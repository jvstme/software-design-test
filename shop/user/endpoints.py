from typing import Annotated

from fastapi import APIRouter, HTTPException, status
from pydantic import BaseModel, Field

from ..admin.auth import admin_auth
from ..db import DB
from ..db import User as UserRecord
from .auth import AuthorizedUser

router = APIRouter()


class User(BaseModel):
    id: int
    username: str

    class Config:
        orm_mode = True


class InputUser(BaseModel):
    username: Annotated[str, Field(min_length=4, max_length=100, example="user")]
    password: Annotated[
        str, Field(min_length=5, max_length=20, regex="[a-z0-9_]+", example="password")
    ]


def check_username_available(username: str, db: DB) -> None:
    select_query = db.query(UserRecord.id).where(UserRecord.username == username)
    exists = db.execute(select_query).one_or_none()

    if exists:
        raise HTTPException(
            status.HTTP_409_CONFLICT, f"Username {username!r} is already taken"
        )


@router.get("/", dependencies=[admin_auth])
def list_users(db: DB) -> list[User]:
    query = db.query(UserRecord)
    records = db.execute(query).scalars()
    return [User.from_orm(record) for record in records]


@router.get("/me")
def get_owned_user(user: AuthorizedUser, db: DB) -> User:
    return User.from_orm(user)


@router.post("/")
def create_user(user: InputUser, db: DB) -> User:
    check_username_available(user.username, db)

    user_record = UserRecord(**user.dict())
    db.add(user_record)
    db.flush()

    return User.from_orm(user_record)


@router.put("/me")
def update_user(modify_user: InputUser, user: AuthorizedUser, db: DB) -> User:
    if modify_user.username != user.username:
        check_username_available(modify_user.username, db)

    for attr, value in modify_user.dict().items():
        setattr(user, attr, value)

    db.flush()
    return User.from_orm(user)


@router.delete("/me")
def delete_user(user: AuthorizedUser, db: DB) -> User:
    db.delete(user)
    db.flush()
    return User.from_orm(user)
