from typing import Annotated

from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from ..db import DB, User

http_basic = HTTPBasic(scheme_name="User Auth")


def get_authorized_user(
    credentials: Annotated[HTTPBasicCredentials, Depends(http_basic)], db: DB
) -> User:
    query = (
        db.query(User)
        .where(User.username == credentials.username)
        .where(User.password == credentials.password)
    )

    user_record = db.execute(query).scalar_one_or_none()

    if not user_record:
        raise HTTPException(
            status.HTTP_401_UNAUTHORIZED,
            "Incorrect username or password",
            {"WWW-Authenticate": "Basic"},
        )

    return user_record


AuthorizedUser = Annotated[User, Depends(get_authorized_user)]
