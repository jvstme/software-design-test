from functools import lru_cache
from typing import Annotated

from fastapi import Depends
from pydantic import BaseSettings


class EnvironmentSettings(BaseSettings):
    db_url: str = "sqlite:///db.sqlite3"
    admin_username: str = "admin"
    admin_password: str = "password"


@lru_cache
def get_settings():
    return EnvironmentSettings()


Settings = Annotated[EnvironmentSettings, Depends(get_settings)]
