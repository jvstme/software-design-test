from typing import Annotated

from fastapi import Depends
from sqlalchemy import JSON, Text, create_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, Session, mapped_column

from .settings import get_settings

settings = get_settings()
engine = create_engine(settings.db_url)


def create_schema():
    Base.metadata.create_all(engine)


def get_db():
    db = Session(engine)
    try:
        yield db
    except BaseException:
        db.rollback()
        raise
    else:
        db.commit()
    finally:
        db.close()


DB = Annotated[Session, Depends(get_db)]


class Base(DeclarativeBase):
    __table_args__ = {
        "sqlite_autoincrement": True,
    }


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(Text, unique=True)
    password: Mapped[str] = mapped_column(Text)


class OrderId(Base):
    __tablename__ = "order_ids"

    id: Mapped[int] = mapped_column(primary_key=True)


class OrderEvent(Base):
    __tablename__ = "order_events"

    id: Mapped[int] = mapped_column(primary_key=True)
    order_id: Mapped[int] = mapped_column()
    kind: Mapped[str] = mapped_column(Text)
    data: Mapped[str] = mapped_column(JSON)


class Product(Base):
    __tablename__ = "products"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(Text)
    quantity_available: Mapped[int] = mapped_column()
