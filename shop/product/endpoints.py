from typing import Annotated

from fastapi import APIRouter, HTTPException, status
from pydantic import BaseModel, Field

from ..admin.auth import admin_auth
from ..db import DB
from ..db import Product as ProductRecord
from . import service

router = APIRouter()


class Product(BaseModel):
    id: int
    name: str
    quantity_available: int

    class Config:
        orm_mode = True


class InputProduct(BaseModel):
    name: str
    quantity_available: Annotated[int, Field(ge=0)]


class ModifyProduct(BaseModel):
    name: str
    quantity_delta: int


def get_existing_product(id: int, db: DB) -> ProductRecord:
    product = service.get_product(id, db)

    if not product:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"Product {id} not found")

    return product


@router.get("/")
def list_products(db: DB) -> list[Product]:
    query = db.query(ProductRecord)
    records = db.execute(query).scalars()
    return [Product.from_orm(record) for record in records]


@router.get("/{id}")
def get_product(id: int, db: DB) -> Product:
    product = get_existing_product(id, db)
    return Product.from_orm(product)


@router.post("/", dependencies=[admin_auth])
def create_product(product: InputProduct, db: DB) -> Product:
    product_record = ProductRecord(**product.dict())
    db.add(product_record)
    db.flush()
    return Product.from_orm(product_record)


@router.put("/{id}", dependencies=[admin_auth])
def update_product(id: int, product: ModifyProduct, db: DB) -> Product:
    product_record = get_existing_product(id, db)
    if product.quantity_delta > 0:
        service.increase_quantity_available(product_record, product.quantity_delta)
    else:
        if not service.try_decrease_quantity_available(
            product_record, -product.quantity_delta
        ):
            raise HTTPException(
                status.HTTP_409_CONFLICT,
                (
                    f"Cannot decrease quantity of product {product_record.id} "
                    f"by {-product.quantity_delta} items, only "
                    f"{product_record.quantity_available} items available"
                ),
            )

    for attr, value in product.dict().items():
        if attr != "quantity_delta":
            setattr(product_record, attr, value)

    db.flush()

    return Product.from_orm(product_record)


@router.delete("/{id}", dependencies=[admin_auth])
def delete_product(id: int, db: DB) -> Product:
    product_record = get_existing_product(id, db)
    db.delete(product_record)
    db.flush()
    return Product.from_orm(product_record)
