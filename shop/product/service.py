from ..db import DB, Product


def get_product(id: int, db: DB) -> Product | None:
    query = db.query(Product).where(Product.id == id)
    product = db.execute(query).scalar_one_or_none()
    return product


def increase_quantity_available(product: Product, quantity: int) -> None:
    product.quantity_available += quantity


def try_decrease_quantity_available(product: Product, quantity: int) -> bool:
    if product.quantity_available < quantity:
        return False

    product.quantity_available -= quantity
    return True
