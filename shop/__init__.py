from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from prometheus_fastapi_instrumentator import Instrumentator

from .db import create_schema
from .order.endpoints import router as orders_router
from .product.endpoints import router as products_router
from .user.endpoints import router as users_router

app = FastAPI(title="Shop")
app.include_router(orders_router, prefix="/orders", tags=["orders"])
app.include_router(products_router, prefix="/products", tags=["products"])
app.include_router(users_router, prefix="/users", tags=["users"])

Instrumentator().instrument(app).expose(app)


@app.get("/", include_in_schema=False)
async def index() -> RedirectResponse:
    return RedirectResponse("docs")


create_schema()
