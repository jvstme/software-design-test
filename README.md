# Online Shop

This is a test application developed using event sourcing patterns.

## Install and run

You will need Python 3.11+ and Poetry.

```shell
poetry install
poetry shell
uvicorn shop:app
```

## Use

Open the app hosted by uvicorn in the browser, it should display Swagger UI that you can use to call API methods. Some API methods require Admin or User authorization, so click "Authorize" and enter your credentials before calling them. Default admin credentials are `admin:password`. User credentials can be created with `POST /users/`.

## Prometheus

The app exports several Prometheus metrics. You can test them by running Prometheus with sample configuration.

```shell
docker compose -f prometheus/compose.yml up
```

Prometheus dashboard will be available at `http://localhost:9090/`.

## Contribute

Please, use the linter and the formatter before committing your code.

```shell
ruff .
black .
```
